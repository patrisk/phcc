/*
 * client.c -- a stream socket client demo
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <signal.h>
#include <pthread.h>

#define MAX_PAYLOAD_LEN 512*1024
#define MAXDATASIZE 100 // max number of bytes we can get at once 

static int32_t data_timeout = 1; // Changeable
static unsigned char timed_out = 0;
static unsigned char finished = 0;
static pthread_mutex_t ping = PTHREAD_MUTEX_INITIALIZER;
static int sockfd = 0;

static char *def_port = "8080";
static char *tcp_port;
static int udp_port = 8081;

static void
send_ping(void *data)
{
  char *ip = (char *) data;
  char *rv_ip;
  struct sockaddr_in server_addr;
  uint32_t seq_num;
  uint32_t reply;
  //  int sockfd;
  ssize_t n;
  
  sockfd = socket(AF_INET, SOCK_DGRAM, 0);
  
  inet_aton(ip, &server_addr.sin_addr);
  rv_ip = inet_ntoa(server_addr.sin_addr);
  fprintf(stderr, "Will ping to ip: %s\n", rv_ip);
  
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons(udp_port);

  seq_num = 1;
  reply = 0;

  while (!finished) {
    pthread_mutex_lock(&ping);
    if (timed_out) {
      n = sendto(sockfd, &seq_num, sizeof(uint32_t), 0, (struct sockaddr *) &server_addr, sizeof(server_addr));
      fprintf(stderr, "Sent: %zu bytes\n", n);

      n = recvfrom(sockfd, (void *) &reply, sizeof(uint32_t), MSG_DONTWAIT, NULL, NULL);
      fprintf(stderr, "Received: %zu bytes\n", n);
      
      seq_num++;
    }
    pthread_mutex_unlock(&ping);

    usleep(500000); // sleep for 0.5 seconds
  }
}

// get sockaddr, IPv4 or IPv6:
static void
*get_in_addr(struct sockaddr *sa)
{
  if (sa->sa_family == AF_INET) {
    return &(((struct sockaddr_in*)sa)->sin_addr);
  }

  return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

static void
sig_handler(int signo)
{
  if (signo == SIGINT) {
    fprintf(stderr, "Received SIGINT.\n");
    finished = 1;
  }  
}

static int
bind_socket(char *argv)
{
  int yes=1;
  int sockfd;
  struct addrinfo hints, *servinfo, *p;
  int rv;
  char s[INET6_ADDRSTRLEN];

  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;

  if ((rv = getaddrinfo(argv, tcp_port, &hints, &servinfo)) != 0) {
    fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
    return 1;
  }

  // loop through all the results and connect to the first we can
  for(p = servinfo; p != NULL; p = p->ai_next) {
    if ((sockfd = socket(p->ai_family, p->ai_socktype,
        p->ai_protocol)) == -1) {
      perror("client: socket");
      continue;
    }

    if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
      close(sockfd);
      perror("client: connect");
      continue;
    }

    break;
  }

  if (p == NULL) {
    fprintf(stderr, "client: failed to connect\n");
    return -1;
  }

  if (setsockopt(sockfd, IPPROTO_TCP, TCP_NODELAY, &yes, sizeof(int)) == -1) {
    perror("setsockopt");
    exit(1);
  }


  inet_ntop(p->ai_family, get_in_addr((struct sockaddr *)p->ai_addr),
          s, sizeof s);
  printf("client: connecting to %s\n", s);

  freeaddrinfo(servinfo); // all done with this structure

  return sockfd;
}

static void 
eventloop(int sockfd)
{
  int32_t i;
  int32_t efd, nfds, sleep_time;
  struct epoll_event ev;
  struct epoll_event *events;
  time_t cur_time, next_timeout;

  if((efd = epoll_create(1)) == -1){
    perror("epoll_create");
    return;
  }

  ev.events = EPOLLIN;
  ev.data.fd = sockfd;

  if(epoll_ctl(efd, EPOLL_CTL_ADD, sockfd, &ev) == -1){
    perror("epoll_ctl");
    return;
  }

  next_timeout = time(NULL) + data_timeout;
  events = calloc(64, sizeof events);

  while(!finished){
    cur_time = time(NULL);

    if(cur_time > next_timeout)
      sleep_time = 0;
    else
      sleep_time = next_timeout - cur_time;
    
    nfds = epoll_wait(efd, events, 1, sleep_time*1000);
    if(nfds == -1){

      fprintf(stderr, "epoll_wait() failed\n");      
      break;

    } else if(nfds == 0){
      next_timeout = time(NULL) + data_timeout;
      fprintf(stderr, "Timed out.\n");
      if (!timed_out) {
	pthread_mutex_lock(&ping);
	timed_out = 1;
	pthread_mutex_unlock(&ping);
      }

    } else{
      pthread_mutex_lock(&ping);

      for (i = 0; i <= nfds; ++i) {
        // Event on the connection.
        if (sockfd == events[i].data.fd) {
          // Fetch data from socket
          char buf[MAX_PAYLOAD_LEN];
          ssize_t bytes_read;
          bytes_read = recv(sockfd, &buf, MAX_PAYLOAD_LEN, 0);  
          if (bytes_read <= 0) {
            fprintf(stderr, "\n");
            fprintf(stderr, "Server disconnected.\n");
	    break;
          }
	  
	  timed_out = 0;
	  fprintf(stderr, ".");
          next_timeout = time(NULL) + data_timeout;
        }
      }
      
      pthread_mutex_unlock(&ping);
    }
  }
  
  free(events);
}

int
main(int argc, char *argv[])
{
  pthread_t udp_thread;
  int sockfd;
  int rv;

  if (argc < 2) {
      fprintf(stderr,"usage: client hostname <tcp port> <udp port>\n");
      exit(1);
  }
  
  tcp_port = def_port;

  if (argc == 4) {
    tcp_port = argv[2];
    udp_port = atoi( argv[3] );
  }

  fprintf(stderr, "If we don't receive a packet in %d seconds, we will send a ping command to the server.\n", data_timeout);

  if (signal(SIGINT, sig_handler) == SIG_ERR) {
    fprintf(stderr, "Can't catch SIGINT.\n");
  }

  sockfd = bind_socket(argv[1]);
  if (sockfd == -1) {
    fprintf(stderr, "client: failed to create socket.\n");
    exit(1);
  }
  
  if (pthread_create(&udp_thread, NULL, (void *) send_ping, argv[1])) {
    fprintf(stderr, "Error creating \"send_ping\" thread.\n");
    close(sockfd);
    exit(1);
  }
  
  eventloop(sockfd);

  //  rv = shutdown(sockfd, SHUT_RD);
  //fprintf(stderr, "shutdown() -> %d\n", rv);

  rv = close(sockfd);
  fprintf(stderr, "close() -> %d\n", rv);

  if (pthread_join(udp_thread, NULL)) {
    fprintf(stderr, "Error joining udp_thread thread.\n");
    exit(1);
  }

  pthread_mutex_destroy(&ping);

  exit(0);

  return 0;
}
