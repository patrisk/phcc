/*
 * server.c -- a stream socket server demo
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <sys/epoll.h>
#include <netinet/tcp.h>
#include <pthread.h>
#include <time.h>

#define MAX_PAYLOAD_LEN 512*1024
#define BACKLOG 10     // how many pending connections queue will hold

static char *def_port = "8080";
static char *tcp_port;
static int udp_port = 8081;

static void
*ping_wait(void *data)
{
  int sockfd;
  int rv;
  int n;
  uint32_t seq_nr;
  struct sockaddr_in server_addr, client_addr;
  socklen_t len;

  memset(&server_addr, 0, sizeof(struct sockaddr_in));
  bzero(&server_addr, sizeof(server_addr));

  sockfd = socket(AF_INET, SOCK_DGRAM, 0);
  fprintf(stderr, "sockfd == %d\n", sockfd);

  server_addr.sin_family = AF_INET;
  server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  server_addr.sin_port = htons(udp_port);

  rv = bind(sockfd, (struct sockaddr *) &server_addr, sizeof(server_addr));
  if (rv < 0) {
    fprintf(stderr, "Failed to bind UDP socket to port.\n");
    exit(1);
  }
  
  fprintf(stderr, "UDP socket ready.\n");

  while (1) {
    len = sizeof(client_addr);
    /* Wait for ping */
    n = recvfrom(sockfd, (void *) &seq_nr, sizeof(uint32_t), 0, (struct sockaddr *) &client_addr, &len);
    fprintf(stderr, "Received a ping: %d bytes with seq: %x\n", n, seq_nr);

    if (errno & EBADF) {
      fprintf(stderr, "errno -> EBADF\n");
    } else if (errno & ECONNREFUSED) {
      fprintf(stderr, "errno -> ECONNREFUSED\n");
    } else if (errno & EFAULT) {
      fprintf(stderr, "errno -> EFAULT\n");
    } else if (errno & EINTR) {
      fprintf(stderr, "errno -> EINTR\n");
    } else if (errno & EINVAL) {
      fprintf(stderr, "errno -> EINVAL\n");
    } else if (errno & ENOMEM) {
      fprintf(stderr, "errno -> ENOMEM\n");
    } else if (errno & ENOTCONN) {
      fprintf(stderr, "errno -> ENOTCONN\n");
    } else if (errno & ENOTSOCK) {
      fprintf(stderr, "errno -> ENOTSOCK\n");
    } else if (errno) {
      fprintf(stderr, "errno -> ???\n");
    } else {
      break; // Exit time.
    }

    /* Send a reply */
    n = sendto(sockfd, &seq_nr, sizeof(uint32_t), 0, (struct sockaddr *) &client_addr, sizeof(client_addr));
    fprintf(stderr, "Sent a reply: %d bytes.\n", n);
  }

  close(sockfd);
  
  return NULL;
}

static void
*send_data(void *data)
{
  int sockfd = *((int*) data);
  char buf[MAX_PAYLOAD_LEN];

  memset(&buf, 0, MAX_PAYLOAD_LEN);

  while (1) {

    ssize_t bytes_sent = send(sockfd, &buf, MAX_PAYLOAD_LEN, MSG_NOSIGNAL);
    if (bytes_sent < 0) {
      fprintf(stderr, "Sent: %zd bytes.\n", bytes_sent);
      close(sockfd);

      switch (errno) {
      case ECONNRESET: 
	fprintf(stderr, "Client closed connection.\n");
	break;

      case EINTR: break;
      case ENOBUFS: 
	fprintf(stderr, "Output queue is full. Generally does not occur according to the man pages.\n");
	break;
      case ENOMEM: 
	fprintf(stderr, "Memory is full.\n");
	break;
      case EPIPE:
	fprintf(stderr, "Program shutting down.\n");
	break;
      }
     
      break;
    }
  }

  return NULL;
}

static void
sigchld_handler(int s)
{
  while(waitpid(-1, NULL, WNOHANG) > 0);
}

// get sockaddr, IPv4 or IPv6:
static void 
*get_in_addr(struct sockaddr *sa)
{
  if (sa->sa_family == AF_INET) {
    return &(((struct sockaddr_in*)sa)->sin_addr);
  }

  return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

static int
bind_socket(void)
{
  int sockfd;  // listen on sock_fd
  struct addrinfo hints, *servinfo, *p;
  int yes=1;
  int rv;

  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = AI_PASSIVE; // use my IP

  if ((rv = getaddrinfo(NULL, tcp_port, &hints, &servinfo)) != 0) {
    fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
    return -1;
  }

  // loop through all the results and bind to the first we can
  for(p = servinfo; p != NULL; p = p->ai_next) {
    if ((sockfd = socket(p->ai_family, p->ai_socktype,
        p->ai_protocol)) == -1) {
      perror("server: socket");
      continue;
    }

    if (setsockopt(sockfd, IPPROTO_TCP, TCP_NODELAY, &yes, sizeof(int)) == -1) {
      perror("setsockopt");
      exit(1);
    }

    if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
      close(sockfd);
      perror("server: bind");
      continue;
    }

    break;
  }

  if (p == NULL)  {
    fprintf(stderr, "server: failed to bind\n");
    return -1;
  }

  freeaddrinfo(servinfo); // all done with this structure

  return sockfd;
}

static void 
eventloop(int sockfd)
{
  socklen_t sin_size;
  struct sockaddr_storage their_addr;
  char s[INET6_ADDRSTRLEN];
  pthread_t socket_thread;
  pthread_t udp_thread;

  // Start udp thread
  if (pthread_create(&udp_thread, NULL, ping_wait, NULL)) {
    fprintf(stderr, "Error creating \"ping_wait\" thread.\n");
    return;
  }

  while(1) {  // main accept() loop
    int32_t socket_fd;

    sin_size = sizeof (their_addr);
    fprintf(stderr, "Waiting for new connections.\n");

    socket_fd = accept(sockfd, (struct sockaddr *)&their_addr, &sin_size);
    if (socket_fd == -1) {
        perror("accept");
        continue;
    }

    inet_ntop(their_addr.ss_family, get_in_addr((struct sockaddr *)&their_addr), s, sizeof s);
    fprintf(stderr, "server: got connection from %s\n", s);

    // Create the sending thread
    
    if (pthread_create(&socket_thread, NULL, send_data, &socket_fd)) {
      fprintf(stderr, "Error creating \"send_data\" thread.\n");
      break;
    }

    if (pthread_join(socket_thread, NULL)) {
      fprintf(stderr, "Error joining socket_thread thread.\n");
      break;
    }
  }
  
  if (pthread_join(udp_thread, NULL)) {
    fprintf(stderr, "Error joining udp_thread thread.\n");
  }
}

int
main(int argc, char *argv[])
{
  int sockfd;  // listen on sock_fd, new connection on new_fd
  struct sigaction sa;

  tcp_port = def_port;

  if (argc == 3) {
    tcp_port = argv[1];
    udp_port = atoi( argv[2] );
  }

  if ((sockfd = bind_socket()) < 0) {
    perror("bind");
    exit(1);
  }

  if (listen(sockfd, BACKLOG) == -1) {
    perror("listen");
    exit(1);
  }

  // Sigterm
  sa.sa_handler = sigchld_handler; // reap all dead processes
  sigemptyset(&sa.sa_mask);
  sa.sa_flags = SA_RESTART;

  if (sigaction(SIGCHLD, &sa, NULL) == -1) {
    perror("sigaction");
    exit(1);
  }

  printf("server: waiting for connections...\n");
  eventloop(sockfd);

  return 0;
}
