# README #

### Compile ###

        gcc server.c -o server -lpthread
        gcc client.c -o client -lpthread

The server requires no additional parameters and the client must be given the hostname followed by TCP and UDP port.

These can be overridden when starting the server, but default to 8080 and 8081 respectively. 